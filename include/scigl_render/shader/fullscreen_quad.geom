/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

R""(
#version 330 core
layout(points) in;
layout(triangle_strip, max_vertices = 4) out;
out vec2 texture_coordinate;

// Creates a fullscreen quad with z-Coordinate 1.0, so stuff can be rendered in
// front of the texture :). Memberberries: looking down z the negative axis.
void main()
{
  gl_Position = vec4(1.0, 1.0, 1.0, 1.0);
  texture_coordinate = vec2(1.0, 1.0);
  EmitVertex();
  gl_Position = vec4(-1.0, 1.0, 1.0, 1.0);
  texture_coordinate = vec2(0.0, 1.0);
  EmitVertex();
  gl_Position = vec4(1.0, -1.0, 1.0, 1.0);
  texture_coordinate = vec2(1.0, 0.0);
  EmitVertex();
  gl_Position = vec4(-1.0, -1.0, 1.0, 1.0);
  texture_coordinate = vec2(0.0, 0.0);
  EmitVertex();
  EndPrimitive();
})""