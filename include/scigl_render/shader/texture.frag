/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

R""(
#version 330 core
struct Material
{
  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
  float shininess;
};

in vec3 world_position;
in vec3 normal;
in vec2 texture_coordinate;

uniform vec3 camera_position;
uniform vec3 light_position;
uniform vec3 light_color;
uniform Material material;
uniform sampler2D texture0;
uniform float diffuse_texture_strength;

out vec4 fragment_color;

// displays a material + texture
// supports ambient, diffuse and specular lighting
void main()
{
  vec3 light_direction = normalize(light_position - world_position);
  // ambient
  vec3 ambient = material.ambient * light_color;
  // diffuse
  vec3 diffuse_color = material.diffuse;  // + diffuse_texture_strength * texture(texture0, texture_coordinate).xyz;
  float diff = max(dot(normal, light_direction), 0.0);
  vec3 diffuse = diff * diffuse_color * light_color;
  // specular
  vec3 view_direction = normalize(camera_position - world_position);
  vec3 reflection_direction = reflect(-light_direction, normal);
  float spec = pow(max(dot(view_direction, reflection_direction), 0.0), 
                   material.shininess);
  vec3 specular = spec * material.specular * light_color;
  // combined
  vec3 result = ambient + diffuse + specular;
  fragment_color = vec4(result, 1.0);
}
)""