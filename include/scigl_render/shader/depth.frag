/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

R""(
#version 330 core
in float depth;
out float positive_depth;

void main()
{
  // since we are looking down the negative axis
  positive_depth = -depth;
})""