/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <GL/gl3w.h>
#include <glm/glm.hpp>
#include <string>

namespace scigl_render
{
/*!
Wraps a shader program for simplified compilation and usage.
*/
class Shader
{
public:
  /*! Use set_program_id */
  Shader();

  /*! For convenience. Alternative: default constructor + set_program_id */
  Shader(GLuint program_id);

  /*! Activate this shader program. */
  void activate() const;

  /*! Deactivate this shader program. */
  void deactivate() const;

  /*! Set the id of the underlying shader program */
  void set_program_id(GLuint id);

  /*! Get the uniform variable value from the shader program. */
  bool getBool(const std::string &name) const;

  /*! Get the uniform variable value from the shader program. */
  int getInt(const std::string &name) const;

  /*! Get the uniform variable value from the shader program. */
  uint32_t getUInt(const std::string &name) const;

  /*! Get the uniform variable value from the shader program. */
  float getFloat(const std::string &name) const;

  /*! Set the uniform variable value in the shader program. */
  void setBool(const std::string &name, bool value) const;

  /*! Set the uniform variable value in the shader program. */
  void setInt(const std::string &name, int value) const;

  /*! Set the uniform variable value in the shader program. */
  void setUInt(const std::string &name, uint32_t value) const;

  /*! Set the uniform variable value in the shader program. */
  void setFloat(const std::string &name, float value) const;

  /*! Set the uniform variable value in the shader program. */
  void setVec2(const std::string &name, const glm::vec2 &value) const;

  /*! Set the uniform variable value in the shader program. */
  void setVec2(const std::string &name, float x, float y) const;

  /*! Set the uniform variable value in the shader program. */
  void setVec3(const std::string &name, const glm::vec3 &value) const;

  /*! Set the uniform variable value in the shader program. */
  void setVec3(const std::string &name, float x, float y, float z) const;

  /*! Set the uniform variable value in the shader program. */
  void setVec4(const std::string &name, const glm::vec4 &value) const;

  /*! Set the uniform variable value in the shader program.  */
  void setVec4(const std::string &name, float x, float y, float z, float w)
      const;

  /*! Set the uniform variable value in the shader program. */
  void setMat2(const std::string &name, const glm::mat2 &mat) const;

  /*! Set the uniform variable value in the shader program. */
  void setMat3(const std::string &name, const glm::mat3 &mat) const;

  /*! Set the uniform variable value in the shader program. */
  void setMat4(const std::string &name, const glm::mat4 &mat) const;

  /*! Set the uniform variable value in the shader program. */
  void setUVec2(const std::string &name, const glm::uvec2 &value) const;

  /*! Set the uniform variable value in the shader program. */
  void setUVec3(const std::string &name, const glm::uvec3 &value) const;

  /*! Set the uniform variable value in the shader program. */
  void setUVec4(const std::string &name, const glm::uvec4 &value) const;

  /*! Set the uniform variable value in the shader program. */
  void setUVec2(const std::string &name, uint32_t x, uint32_t y) const;

  /*! Set the uniform variable value in the shader program. */
  void setUVec3(const std::string &name,
                uint32_t x, uint32_t y, uint32_t z) const;

  /*! Set the uniform variable value in the shader program. */
  void setUVec4(const std::string &name,
                uint32_t x, uint32_t y, uint32_t z, uint32_t w) const;

  /*! Set the uniform variable value in the shader program. */
  void setFloatArray(const std::string &name, const float array[], int count) const;

private:
  /*!
  Reference to this shader program.
  */
  GLuint program_id;
};
} // namespace scigl_render
