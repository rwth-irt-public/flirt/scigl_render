/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <GL/gl3w.h>
#include <scigl_render/shader/shader.hpp>
#include <string>

namespace scigl_render
{
/*!
Builds a shader program
*/
class ShaderBuilder
{
public:
  ShaderBuilder();

  /*!
  Attach a vertex shader to the program.
  */
  void attach_vertex_shader(const std::string &vs_source);

  /*!
  Attach a fragment shader to the program.
  */
  void attach_fragment_shader(const std::string &fs_source);

  /*!
  Attach a geometry shader to the program.
  */
  void attach_geometry_shader(const std::string &gs_source);

  /*!
  Attach tessellation control and evaluation shaders to the program
  \param tcs_source the source code of the tessellation control shader
  \param tes_source the source code of the tessellation evaluation shader
  */
  void attach_tessellation_shader(const std::string &tcs_source,
                                  const std::string &tes_source);

  /*!
  Attach a compute shader to the program.
  */
  void attach_compute_shader(const std::string &cs_source);

  /*!
  Builds the shader program.
  */
  Shader build();

private:
  GLuint vertex_shader, fragment_shader, geometry_shader, tc_shader, te_shader,
      compute_shader;

  /*!
  Compiles the shader source code.
  \param source the buffered source code
  \param shader_type for example GL_VERTEX_SHADER & GL_FRAGMENT_SHADER
  \return the reference id of the compiled shader object
  */
  GLuint compile_shader(const std::string &source, GLenum shader_type);

  /*!
  Logs the compilation error info.
  \param shader the reference id of the shader
  \param source the source code of the shader
  */
  void checkShaderCompileErrors(GLuint shader, const std::string &source);

  /*!
  Logs the compilation error info.
  */
  void checkProgramCompileErrors(GLuint program);
};
} // namespace scigl_render