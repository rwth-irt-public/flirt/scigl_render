/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

R""(
#version 330 core
in vec3 normal;
in vec3 world_position;

uniform float ambient_strength;
uniform vec3 light_color;
uniform vec3 light_position;
uniform vec3 object_color;

out vec4 fragment_color;

// displays the scene in a single diffuse color
void main()
{
  // ambient
  vec3 ambient = ambient_strength * light_color;
  vec3 light_direction = normalize(light_position - world_position);
  // diffuse
  vec3 normalized_normal = normalize(normal);
  float diff = max(dot(normalized_normal, light_direction), 0.0);
  vec3 diffuse = diff * light_color;    
  vec3 result = (ambient + diffuse) * object_color;
  fragment_color = vec4(result, 1.0);
}
)""