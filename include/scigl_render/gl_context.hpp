/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

namespace scigl_render
{
/*!
This is the entry to create any application with scigl_render. It manages
loading the OpenGL functions and then a GLFW context.
Bundles the creation and destruction of the GLFW render context RAII style.
The context is basically a GLFW window, so some methods for convenient handling
of the window are provided.
The copy constructor is deleted, use a shard_ptr.
*/
class GLContext
{
public:
  GLContext(bool visible, bool fullscreen, int width, int height, int version_major = 4, int version_minor = 3);
  GLContext(const GLContext&) = delete;
  ~GLContext();

  /*!
  \return the underlying window.
  */
  GLFWwindow* get_window();

  int get_width();
  int get_height();

  /*!
  \returns true if the fullscreen mode is active, false otherwise
  */
  bool is_fullscreen();

  /*!
  Enable or disable fullscreen mode
  \param enabled if it is true fullscreen will be activated otherwise
  windowed mode is enabled
  */
  void set_fullscreen(bool enabled);

  /*!
  Switches to fullscreen on the screen where the largest area of the current
  window is located.
  If fullscreen is enabled it switches back to windowed mode.
  */
  void toggle_fullscreen();

  /*!
  Explicity set the resolution of the render target.
  In window mode the size is changed. In fullscreen mode the resolution is
  changed.
  */
  void set_resolution(int width, int height);

  /*!
  Set the resolution to the resolution of the monitor.
  */
  void set_monitor_resolution();

private:
  GLFWwindow* window;
  int width, height;

  /*!
  Finds the monitor in which the window is located
  */
  GLFWmonitor* find_current_monitor();
};
}  // namespace scigl_render