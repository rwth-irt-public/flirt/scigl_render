/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <scigl_render/buffer/frame_buffer.hpp>
#include <array>
#include <functional>
#include <memory>

namespace scigl_render
{
/*!
Transfer data from a framebuffer to the CPU.
Two pixel buffer objects are use like a double buffer: one PBO is filled
asynchronously with data from the frame buffer while the other one is ready to
be mapped to the clients memory. Consequently the results of a rendering is
delayed by one read call. 
*/
class FramebufferReader
{
public:
  /*!
  Renders the scene off-screen and calculates the depth values.
  \param buffer read from this framebuffer
  \param pixel_size the size of each pixel: number_channels * sizeof(type)
  */
  FramebufferReader(std::shared_ptr<FrameBuffer> buffer,
                    size_t pixel_size);

  /*!
  Starts reading from the FBO to the backbuffer PBO. This operation is 
  asynchronous.
  */
  void start_read() const;

  /*!
  Reads synchronously from the frontbuffer PBO. Operate on this data while
  another read is running on the backbuffer :)
  \retuns might return NULL otherwise the data
  */
  void *do_read() const;

  /*!
  Unmaps the OpenGL memory.
  */
  void end_read() const;

  /*! Swap the front- and backbuffer */
  void swap_buffers();

private:
  std::shared_ptr<FrameBuffer> framebuffer;
  // Two pbos one to read to the other one to map form, alternating
  std::array<GLuint, 2> pbos;
  // transfer from fbo to pbo via glReadPixels
  int backbuffer_index = 0;
  // transfer from pbo to CPU memory
  int frontbuffer_index = 1;
};
} // namespace scigl_render
