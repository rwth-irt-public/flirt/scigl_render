/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <GL/gl3w.h>
#include <memory>
#include <scigl_render/buffer/texture2d.hpp>

namespace scigl_render {
/*!
Wraps a texture frame buffer that can be rendered to offscreen.
has a color and depth-stencil attachement.
The resources are managed RAII style so the copy constructor is disables; use
a shared_ptr to share the isntance.
*/
class FrameBuffer {
 public:
  /*!
  Creates the framebuffer with a texture attachment to render to.
  \throws a runtime_error on failure
  \param texture the texture to render to
  */
  FrameBuffer(std::shared_ptr<Texture2D> texture);
  FrameBuffer(const FrameBuffer &) = delete;
  ~FrameBuffer();

  /*!
  Use this framebuffer for rendering, sets the color mask and depth testing.
  */
  void activate() const;

  /*!
  Do not use this framebuffer for rendering anymore.
  */
  void deactivate() const;

  /*!
  Clears the color and depth values.
  \param color (e.g. absolute depth) using default OpenGL value 0
  \param depth (for depth testing) using default OpenGL value 1
  \param stencil (what to ignore) using OpenGL default value 0
  */
  void clear(float color = 0, float depth = 1, int stencil = 0) const;

  /** returns the render target texture */
  std::shared_ptr<Texture2D> get_texture() const;

  /** gets the maximum rbo size */
  static int get_max_size();

  /**
   * Reads the image from the underlying texture
   * \param pixels stores the values in this adress
   * \param x window x coordinates of the first pixel
   * \param y window y coordinates of the first pixel
   * \param width horizontal dimension of the image
   * \param height vertical dimension of the image
   */
  void read_image(GLvoid *pixels, GLint x, GLint y, GLsizei width,
                  GLsizei height) const;

 private:
  // framebuffer object to render the texture into
  GLuint fbo;
  // texture for renderng colors, can be sampled from
  std::shared_ptr<Texture2D> color_tex;
  // renderbuffer object for the depth testing, does not allow sampling
  GLuint depth_stencil_rbo;
};
}  // namespace scigl_render