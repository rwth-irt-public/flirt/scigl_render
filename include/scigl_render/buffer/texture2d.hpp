/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <GL/gl3w.h>

namespace scigl_render {
/** RAII style creation of a 2D texture */
class Texture2D {
 public:
  Texture2D(GLsizei width, GLsizei height, GLenum format,
            GLenum internal_format, GLenum type);
  Texture2D(const Texture2D &) = delete;
  ~Texture2D();

  /** activates texture_n for use in shaders */
  static void activate(GLenum texture_n);

  /**
   * bind this texture to an image unit for imageLoad
   * \param unit the binding unit
   * \param access GL_READ_ONLY, GL_WRITE_ONLY, or GL_READ_WRITE
   */
  void bind_image_unit(GLuint unit, GLenum access) const;

  /** binds this texture to GL_TEXTURE_2D */
  void bind() const;

  /** unbinds this texture */
  void unbind() const;

  /**
   * Reads the texture in the pixels pointer.
   * Note: If a non-zero named buffer object is bound to the
   * GL_PIXEL_PACK_BUFFER target (see glBindBuffer) while a texture image is
   * specified, pixels is treated as a byte offset into the buffer object's
   * data store.
   */
  void read_image(GLvoid *pixels) const;

  /** upload an image to the texture. */
  void store_image(const GLvoid *image) const;

  GLuint get_raw() const;
  GLsizei get_width() const;
  GLsizei get_height() const;
  GLenum get_format() const;
  GLenum get_internal_format() const;
  GLenum get_type() const;

 private:
  const GLsizei width, height;
  const GLenum format, internal_format, type;
  GLuint texture;
};
}  // namespace scigl_render