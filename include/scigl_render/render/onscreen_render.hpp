/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <memory>
#include <scigl_render/scene/cv_camera.hpp>
#include <scigl_render/scene/diffuse_light.hpp>
#include <scigl_render/gl_context.hpp>
#include <scigl_render/scene/model.hpp>
#include <scigl_render/shader/shader.hpp>

namespace scigl_render
{
/*!
Simple rendering of a scene right on the screen (GLContext window).
*/
class OnscreenRender
{
public:
  /*! Need to set the shader with the setter */
  OnscreenRender();

  /*!
  Configures the rendering environment and loads the models.
  \param shader how to render the scene
  */
  OnscreenRender(Shader shader);

  /*! Shader program for rendering the scene*/
  void set_shader_program(Shader shader);

  /*!
  Renders the next frame onto the screen.
  */
  void next_frame(GLContext &gl_context, const CvCamera &camera,
                  const Model &model, const DiffuseLight &light);

private:
  // renders the 3D scene
  Shader shader;
};
} // namespace scigl_render