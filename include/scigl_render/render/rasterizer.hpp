/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <cstddef>
#include <utility>

namespace scigl_render {
/**
 * Render different views to a rasterized texture. This class is double buffer
 * enabled.
 */
class Rasterizer {
 public:
  /**
   * Defines a header that is compatible to the OpenCV Mat_<T> constructor.
   */
  template <typename T>
  struct ImageHeader {
    /** height of the image */
    size_t rows;
    /** width of the image */
    size_t columns;
    /** points to the first pixel of the image in the texture */
    T *data;
    /** size in bytes of one row in the image */
    size_t step;
  };

  Rasterizer();

  /**
   * Initialize the rasterizer with 2D dimensions. Use calc_min_raster to
   * determine the minimal dimensions for a given number of views.
   * \param width the width of one view
   * \param height the height of one view
   * \param views_per_row the number of views in one row
   * \param views_per_column the number of views in one column
   * \param internal_format internal OpenGl texture format
   */
  Rasterizer(size_t width, size_t height, size_t views_per_row,
             size_t views_per_column);

  /** activate a view by its 2D coordinates */
  void activate_view(size_t row, size_t column) const;

  /** as if the rows were concatenated */
  void activate_view(size_t view) const;

  /** activate the whole texture view, for example when clearing it */
  void activate_all() const;

  /**
   * Minimal raster size for the given number of views
   * \param n_views total number of views to render, must be greater than 0
   * \param width of the observation image
   * \param height of the observation image
   * \param max_buffer_size the maximum dimensions of the buffer (e.g. RBO)
   * \retuns views per row, views per column. Both 0 if the views do not fit
   * into the buffer.
   */
  static std::pair<size_t, size_t> calc_min_raster(size_t n_views, size_t width,
                                                   size_t height,
                                                   size_t max_buffer_size);

  /**
   * Create metadata to read one view as image from the texture of all views.
   * \param row of the view
   * \param column of the view
   * \param data pointer to the full texture
   * \param channels number of channels in the image
   */
  template <typename T>
  ImageHeader<T> get_image(size_t row, size_t column, void *data,
                           size_t channels) const {
    auto xy = view_xy(row, column);
    // all the rows above and the columns in front
    ImageHeader<T> header;
    header.data = static_cast<T *>(data) +
                  (xy.second * texture_width + xy.first) * channels;
    header.columns = width;
    header.rows = height;
    header.step = texture_width * channels * sizeof(T);
    return header;
  }

  /**
   * Create metadata to read one view as image from the texture of all views.
   * \param view get the image for this view
   * \param data pointer to the full texture
   * \param channels number of channels in the image
   */
  template <typename T>
  ImageHeader<T> get_image(size_t view, void *data, size_t channels) const {
    auto pos = view_row_column(view);
    return get_image<T>(pos.first, pos.second, data, channels);
  }

  size_t get_views_per_row() const;
  size_t get_views_per_column() const;
  size_t get_view_width() const;
  size_t get_view_height() const;
  /** view width * view height */
  size_t get_view_size() const;
  size_t get_texture_width() const;
  size_t get_texture_height() const;

  /** position of the first pixel of the image in the texture */
  std::pair<size_t, size_t> get_view_xy(size_t view) const;
  void set_views_per_row(size_t views_per_row);
  void set_views_per_column(size_t views_per_column);
  void set_view_width(size_t width);
  void set_view_height(size_t height);

 private:
  // dimensions of the observation
  size_t width, height;
  // for the whole texture precalculated
  size_t views_per_row, views_per_column;
  size_t texture_width, texture_height;

  /**
   * Calculates row and column of the view
   */
  std::pair<size_t, size_t> view_row_column(size_t view) const;

  /**
   * Calculates x and y coordinates of the row and column
   * \returns x,y
   */
  std::pair<size_t, size_t> view_xy(size_t row, size_t column) const;
};
}  // namespace scigl_render
