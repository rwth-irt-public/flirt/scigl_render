# Table of Contents
[[_TOC_]]

# About
Library to simplify rendering objects via OpenGL.
The intendet use case is scientific (e.g. probabalistic filtering).
This library is primarily used with ROS but since it is a plain CMake package ROS is not necessarily required.
However, compiling and installing the dependencies manually requires more work and is not described here.

![Screenshot of scigl_viewer](images/scigl_viewer.png)

*Screenshot of the example application scigl_viewer.*

# Requirements
An OpenGL driver supporting core profile 4.2 is required.
NVIDIA drivers seem to work well, we experienced some weird issues with the Intel Mesa drivers.
You can check your version via:
```bash
glxinfo | grep 'version'
```
We use glTexStorage2D but it could be replaced with the older glTexImage2D, see https://www.khronos.org/opengl/wiki/Texture_Storage for more information).

# Setup
We provide some Blender 3D-models which can be used to test this library.
To clone these models, [Git LFS](https://git-lfs.github.com/) has to be installed first.
Moreover we use the [wstool](http://wiki.ros.org/wstool) to download sources which are only available in repositories.

```bash
# install Git LFS
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
# install wstool
sudo apt install python-wstool
```

Now we can setup the a catkin workspace via the wstool.
You might want to change the URL whether you use SSH or HTTPS to clone repositories.
```bash
# init workspace
mkdir -p my_catkin_ws && cd my_catkin_ws
wstool init src 
# add scigl_render
wstool set -y -t src --git scigl_render https://gitlab.com/rwth-irt-public/flirt/scigl_render.git
wstool update -t src
# add and download dependencies of scigl_render
wstool merge -t src src/scigl_render/.rosinstall
wstool update -t src
```

After setting up the workspace, the system dependencies can be installed via rosdep:
```bash
rosdep install --from-paths src --ignore-src -r -y
```

# Build the Catkin Workspace
As mentioned earlier, you could also use plain CMake to compile this library.
Compiling it in a catkin workspace simplifies compiling the dependencies (gl3w_ros) and linking with them.

As `catkin_make` does not support mixed CMake and catkin workspaces, you must use either `catkin_make_isolated` oder `catkin build` to compile the workspace.
We recommend the latter for faster compile times on multi-core system.
It is part of the [catkin_tools](https://catkin-tools.readthedocs.io/en/latest/index.html).

# Usage
Examples can be found in the *src/example* folder.
Run the *scigl_viewer* or the *scigl_depth_viewer* by specifying the model to display as a command line argument.
Assimp has some problems loading blender files generated with version 2.8 or later.
Export the models as COLLADA (.dae) file to circumvent this problem.
```bash
# run scigl_viewer with coordinates.dae
./build/scigl_render/scigl_viewer src/scigl_render/3d_models/coordinates.dae 
# run scigl_depth_viewer with monkey.dae
./build/scigl_render/scigl_depth_viewer src/scigl_render/3d_models/monkey.dae 
```

The *scigl_viewer* example demonstrates:
- rendering on-screen
- setting up a scene containing a model, camera and light
- using shaders to render the colorized vertices

The *scigl_depth_viewer* example demonstrates:
- rendering off-screen
- rendering multiple views into one texture via viewport rasterization
- render the depth value instead of a model color

![Screenshot of scigl_depth_viewer](images/scigl_depth_viewer.png)

*Screenshot of the example application scigl_viewer.*
*The* depth values are stored in the red channel.*

# Dependencies
Installable via rosinstall:
- [gl3w_ros](https://github.com/Tuebel/gl3w_ros.git) - ROS wrapper for Loading OpenGL core profile

The other dependencies are pulled in via rosdep:
- glfw3 (http://www.glfw.org/) - Provides OpenGL context
- assimp (http://www.assimp.org/) - Loading diverse 3D-model formates
- glm (https://glm.g-truc.net/) - OpenGL math library

# Continous Integration
A gitlab CI pipeline is implemented, which is based on [ros_gitlab_ci](https://gitlab.com/VictorLamoine/ros_gitlab_ci). 
The CI is based on the `ros:melodic-ros-core` docker image and will install the dependencies via rosdep. The gitlab-runner must use a **docker** executor.

# Special Thanks
If you want to learn more about the OpenGL implementation, please visit Joey de Vries website https://learnopengl.com/ and his twitter page https://twitter.com/JoeyDeVriez.
Thanks to his awesome resources I managed to write this library in a bit more than a week.

# Citing
If you use parts of this library in your scientific publication, please consider citing:
```
@article { 3Dcamerabasedmarkerlessnavigationsystemforroboticosteotomies,
      author = "Tim Übelhör and Jonas Gesenhues and Nassim Ayoub and Ali Modabber and Dirk Abel",
      title = "3D camera-based markerless navigation system for robotic osteotomies",
      journal = "at - Automatisierungstechnik",
      year = "01 Oct. 2020",
      publisher = "De Gruyter",
      address = "Berlin, Boston",
      volume = "68",
      number = "10",
      doi = "https://doi.org/10.1515/auto-2020-0032",
      pages=      "863 - 879",
      url = "https://www.degruyter.com/view/journals/auto/68/10/article-p863.xml"
}
```

# Funding
Funded by the Excellence Initiative of the German federal and state governments.
