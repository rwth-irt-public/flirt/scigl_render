/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <algorithm>
#include <scigl_render/buffer/frame_buffer_reader.hpp>
#include <scigl_render/check_gl_error.hpp>
#include <stdexcept>

namespace scigl_render
{
FramebufferReader::FramebufferReader(std::shared_ptr<FrameBuffer> buffer, size_t pixel_size) : framebuffer(buffer)
{
  glGenBuffers(pbos.size(), pbos.data());
  size_t buffer_size = framebuffer->get_texture()->get_width() * framebuffer->get_texture()->get_height() * pixel_size;
  for (size_t i = 0; i < pbos.size(); i++)
  {
    glBindBuffer(GL_PIXEL_PACK_BUFFER, pbos[i]);
    // constantly reading new contents
    glBufferData(GL_PIXEL_PACK_BUFFER, buffer_size, 0, GL_DYNAMIC_READ);
  }
  glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
  check_gl_error("framebuffer-reader created pbos");
}

void FramebufferReader::start_read() const
{
  framebuffer->activate();
  glReadBuffer(GL_COLOR_ATTACHMENT0);
  glBindBuffer(GL_PIXEL_PACK_BUFFER, pbos[backbuffer_index]);
  glReadPixels(0, 0, framebuffer->get_texture()->get_width(), framebuffer->get_texture()->get_height(),
               framebuffer->get_texture()->get_format(), framebuffer->get_texture()->get_type(), nullptr);
  glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
  framebuffer->deactivate();
}

void* FramebufferReader::do_read() const
{
  glBindBuffer(GL_PIXEL_PACK_BUFFER, pbos[frontbuffer_index]);
  return glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);
}

void FramebufferReader::end_read() const
{
  glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
}

void FramebufferReader::swap_buffers()
{
  std::swap(backbuffer_index, frontbuffer_index);
}
}  // namespace scigl_render
