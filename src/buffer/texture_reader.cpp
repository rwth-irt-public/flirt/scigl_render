/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <algorithm>
#include <scigl_render/check_gl_error.hpp>
#include <scigl_render/buffer/texture_reader.hpp>
#include <stdexcept>

namespace scigl_render
{
TextureReader::TextureReader(std::shared_ptr<Texture2D> buffer, size_t pixel_size) : texture(buffer)
{
  glGenBuffers(pbos.size(), pbos.data());
  size_t buffer_size = texture->get_width() * texture->get_height() * pixel_size;
  for (size_t i = 0; i < pbos.size(); i++)
  {
    glBindBuffer(GL_PIXEL_PACK_BUFFER, pbos[i]);
    // constantly reading new contents
    glBufferData(GL_PIXEL_PACK_BUFFER, buffer_size, 0, GL_DYNAMIC_READ);
  }
  glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
  check_gl_error("framebuffer-reader created pbos");
}

void TextureReader::start_read() const
{
  glBindBuffer(GL_PIXEL_PACK_BUFFER, pbos[backbuffer_index]);
  texture->read_image(0);
  glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
}

void* TextureReader::do_read() const
{
  glBindBuffer(GL_PIXEL_PACK_BUFFER, pbos[frontbuffer_index]);
  return glMapBuffer(GL_PIXEL_PACK_BUFFER, GL_READ_ONLY);
}

void TextureReader::end_read() const
{
  glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
}

void TextureReader::swap_buffers()
{
  std::swap(backbuffer_index, frontbuffer_index);
}
}  // namespace scigl_render
