/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <scigl_render/check_gl_error.hpp>
#include <scigl_render/example/depth_offscreen_render.hpp>

namespace scigl_render
{
// I know there is a lot of configuration but it is intendet to be flexible
DepthOffscreenRender::DepthOffscreenRender(std::shared_ptr<GLContext> context, std::shared_ptr<Texture2D> texture,
                                           size_t pixel_size)
  : gl_context(context)
  , fbo_texture(texture)
  , rasterizer(context->get_width() / 2, context->get_height() / 2, 2, 2)
  , image_buffer(context->get_width() * context->get_height() * pixel_size)
{
  framebuffer = std::make_shared<FrameBuffer>(fbo_texture);
  tex_reader.reset(new TextureReader(fbo_texture, pixel_size));
  // create second texture to copy the data to
  quad_texture =
      std::make_shared<Texture2D>(fbo_texture->get_width(), fbo_texture->get_height(), fbo_texture->get_format(),
                                  fbo_texture->get_internal_format(), fbo_texture->get_type());
}

void DepthOffscreenRender::next_frame(std::shared_ptr<DepthSimulator> depth_simulator, const CartesianPose& model_pose,
                                      const CartesianPose& camera_pose)
{
  using namespace std::placeholders;
  check_gl_error("next frame begin");
  // render to cleared fbo
  framebuffer->clear();
  framebuffer->activate();
  check_gl_error("activated fbo");
  // render 4 poses via rasterization
  for (int i = 0; i < 4; i++)
  {
    auto m_pose = model_pose;
    m_pose.orientation.x += i * M_PI_2;
    rasterizer.activate_view(i);
    depth_simulator->render_pose(m_pose.to_quaternion_pose(), camera_pose.to_quaternion_pose());
  }
  // back to default
  rasterizer.activate_all();
  framebuffer->deactivate();

  // read data from texture
  tex_reader->start_read();
  check_gl_error("asynchronous reading to pbo");
  auto data = tex_reader->do_read();
  check_gl_error("synchronous reading from pbo");

  // display the data on screen
  display_data(data);
  tex_reader->end_read();
  tex_reader->swap_buffers();
  check_gl_error("end synchronous reading");
}

void DepthOffscreenRender::display_data(const void* data)
{
  // Test copying the data, buffer_size is in bytes
  memcpy(image_buffer.data(), data, image_buffer.size());
  quad_texture->store_image(data);
  // draw the copy
  glClearColor(0, 0, 0, 0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  texture_render.draw(quad_texture);
  check_gl_error("drawing fullscreen quad");
  // update
  glfwSwapBuffers(gl_context->get_window());
}
}  // namespace scigl_render