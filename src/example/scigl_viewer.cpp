/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <glm/glm.hpp>
#include <stdexcept>
#include <iostream>
#include <scigl_render/gl_context.hpp>
#include <scigl_render/render/onscreen_render.hpp>
#include <scigl_render/scene/cv_camera.hpp>
#include <scigl_render/scene/diffuse_light.hpp>
#include <scigl_render/shader/shader_builder.hpp>
#include <scigl_render/shader/single_color_shader.hpp>
#include <thread>

// Speed of the motion
const float rotational_speed = 0.01;
const float translational_speed = 0.002;

// forward declare callbacks
// get mouse and keyboard inputs
void process_input(std::shared_ptr<scigl_render::GLContext> context, scigl_render::CartesianPose& camera_pose,
                   const scigl_render::CvCamera& camera);

// Debug printing
std::ostream& operator<<(std::ostream& out, const glm::mat4& mat)
{
  out << "[" << mat[0][0] << " " << mat[1][0] << " " << mat[2][0] << " " << mat[3][0] << "\n"
      << mat[0][1] << " " << mat[1][1] << " " << mat[2][1] << " " << mat[3][1] << "\n"
      << mat[0][2] << " " << mat[1][2] << " " << mat[2][2] << " " << mat[3][2] << "\n"
      << mat[0][3] << " " << mat[1][3] << " " << mat[2][3] << " " << mat[3][3] << "]";

  return out;
}

/*!
Provide the model as argument via command line.
*/
int main(int argc, char* argv[])
{
  // Some default parameters
  const int WIDTH = 640;
  const int HEIGHT = 480;
  using namespace scigl_render;
  if (argc < 2)
  {
    throw std::runtime_error("No model file provided. Run as */scigl_viwer <model_filename>!");
  }
  // Setup renderer creates context
  std::shared_ptr<GLContext> context = std::make_shared<GLContext>(true, false, WIDTH, HEIGHT);
  // build the shader program
  const std::string vs_source =
#include <scigl_render/shader/distort.vert>
      "";
  const std::string fs_source =
#include <scigl_render/shader/texture.frag>
      "";
  ShaderBuilder shader_builder;
  shader_builder.attach_vertex_shader(vs_source);
  shader_builder.attach_fragment_shader(fs_source);
  // auto shader = SingleColorShader::create_shader(0.75, 0, 1, 0);
  OnscreenRender onscreen_render;
  onscreen_render.set_shader_program(shader_builder.build());
  // Intrinsics of my shitty webcam
  CameraIntrinsics camera_intrinsics = {};
  camera_intrinsics.near = 0.01;
  camera_intrinsics.far = 15;
  camera_intrinsics.width = 640;
  camera_intrinsics.height = 480;
  camera_intrinsics.c_x = 411;
  camera_intrinsics.c_y = 310;
  camera_intrinsics.f_x = 511;
  camera_intrinsics.f_y = 513;
  float dist_coeffs[] = { 4.1925421198910247e-02,
                          -9.6463442423611379e-02,
                          -2.3391717576772839e-03,
                          5.8792609967242386e-04,
                          4.9171950039135250e-02,
                          0,
                          0,
                          0 };
  std::copy(std::begin(dist_coeffs), std::end(dist_coeffs), std::begin(camera_intrinsics.dist_coeffs));
  CvCamera camera(camera_intrinsics);
  // Test if Cartesian -> Quaternion works
  CartesianPose camera_pose = { glm::vec3(0, 0, 0), glm::vec3(0, 0, 0) };
  camera.pose = camera_pose.to_quaternion_pose();
  DiffuseLight light;
  Model model(argv[1]);
  model.pose.position = glm::vec3(0, 0, 0.5);
  model.pose.orientation.x = 0;
  model.pose.orientation.w = 0;
  light.position = glm::vec3(0, -0.5, 0);
  light.color = glm::vec3(1, 1, 1);

  // main loop
  int old_enter_pressed = GLFW_RELEASE;
  while (!glfwWindowShouldClose(context->get_window()))
  {
    process_input(context, camera_pose, camera);
    // toggle fullscreen
    int new_enter_pressed = glfwGetKey(context->get_window(), GLFW_KEY_ENTER);
    if (new_enter_pressed == GLFW_RELEASE && old_enter_pressed == GLFW_PRESS)
    {
      context->toggle_fullscreen();
      camera.resize_intrinsics(context->get_width(), context->get_height());
      // render.resize_buffers();
    }
    old_enter_pressed = new_enter_pressed;
    camera.pose = camera_pose.to_quaternion_pose();
    onscreen_render.next_frame(*context, camera, model, light);
    glfwPollEvents();
  }
  // Finally release the context
  glfwTerminate();
  return EXIT_SUCCESS;
}

void process_input(std::shared_ptr<scigl_render::GLContext> context, scigl_render::CartesianPose& camera_pose,
                   const scigl_render::CvCamera& camera)
{
  auto window = context->get_window();
  // exit
  if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
  {
    glfwSetWindowShouldClose(window, true);
  }
  // movement
  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
  {
    camera_pose.position.y += translational_speed;
  }
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
  {
    camera_pose.position.y -= translational_speed;
  }
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
  {
    camera_pose.position.x -= translational_speed;
  }
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
  {
    camera_pose.position.x += translational_speed;
  }
  if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
  {
    camera_pose.position.z += translational_speed;
  }
  if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
  {
    camera_pose.position.z -= translational_speed;
  }
  if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS)
  {
    camera_pose.orientation.x += rotational_speed;
  }
  if (glfwGetKey(window, GLFW_KEY_K) == GLFW_PRESS)
  {
    camera_pose.orientation.x -= rotational_speed;
  }
  if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS)
  {
    camera_pose.orientation.y -= rotational_speed;
  }
  if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS)
  {
    camera_pose.orientation.y += rotational_speed;
  }
  if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
  {
    camera_pose.orientation.z -= rotational_speed;
  }
  if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
  {
    camera_pose.orientation.z += rotational_speed;
  }
  if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
  {
    std::cout << "camera info\n\n";
    std::cout << camera.get_projection_matrix() << "\n\n";
    std::cout << camera.get_view_matrix() << "\n\n";
  }
}
