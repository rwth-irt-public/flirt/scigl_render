/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <scigl_render/shader/single_color_shader.hpp>
#include <scigl_render/shader/shader_builder.hpp>

const std::string vs_source =
#include <scigl_render/shader/distort.vert>
    "";
const std::string fs_source =
#include <scigl_render/shader/single_color.frag>
    "";

namespace scigl_render
{
Shader SingleColorShader::create_shader()
{
  ShaderBuilder builder;
  builder.attach_vertex_shader(vs_source);
  builder.attach_fragment_shader(fs_source);
  return builder.build();
}

Shader SingleColorShader::create_shader(float ambient_strength, float r, float g, float b)
{
  auto shader = create_shader();
  set_ambient_strength(shader, ambient_strength);
  set_color(shader, r, g, b);
  return shader;
}

void SingleColorShader::set_ambient_strength(const Shader& shader, float ambient_strength)
{
  shader.activate();
  shader.setFloat("ambient_strength", ambient_strength);
  shader.deactivate();
}

void SingleColorShader::set_color(const Shader& shader, float r, float g, float b)
{
  shader.activate();
  glm::vec3 color(r, g, b);
  shader.setVec3("object_color", color);
  shader.deactivate();
}
}  // namespace scigl_render