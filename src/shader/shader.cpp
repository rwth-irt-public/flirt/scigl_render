/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <scigl_render/shader/shader.hpp>
#include <stdexcept>

namespace scigl_render
{
Shader::Shader()
{
  set_program_id(0);
}

Shader::Shader(GLuint program_id)
{
  set_program_id(program_id);
}

void Shader::activate() const
{
  glUseProgram(program_id);
}

void Shader::deactivate() const
{
  glUseProgram(GL_ZERO);
}

void Shader::set_program_id(GLuint id)
{
  program_id = id;
}

bool Shader::getBool(const std::string& name) const
{
  int value;
  glGetUniformiv(program_id, glGetUniformLocation(program_id, name.c_str()), &value);
  return (bool)value;
}

int Shader::getInt(const std::string& name) const
{
  int value;
  glGetUniformiv(program_id, glGetUniformLocation(program_id, name.c_str()), &value);
  return value;
}

uint32_t Shader::getUInt(const std::string& name) const
{
  GLuint value;
  glGetUniformuiv(program_id, glGetUniformLocation(program_id, name.c_str()), &value);
  return value;
}

float Shader::getFloat(const std::string& name) const
{
  GLfloat value;
  glGetUniformfv(program_id, glGetUniformLocation(program_id, name.c_str()), &value);
  return value;
}

void Shader::setBool(const std::string& name, bool value) const
{
  glProgramUniform1i(program_id, glGetUniformLocation(program_id, name.c_str()), (int)value);
}

void Shader::setInt(const std::string& name, int value) const
{
  glProgramUniform1i(program_id, glGetUniformLocation(program_id, name.c_str()), value);
}

void Shader::setUInt(const std::string& name, uint32_t value) const
{
  glProgramUniform1ui(program_id, glGetUniformLocation(program_id, name.c_str()), value);
}

void Shader::setFloat(const std::string& name, float value) const
{
  glProgramUniform1f(program_id, glGetUniformLocation(program_id, name.c_str()), value);
}

void Shader::setVec2(const std::string& name, const glm::vec2& value) const
{
  glProgramUniform2fv(program_id, glGetUniformLocation(program_id, name.c_str()), 1, &value[0]);
}

void Shader::setVec2(const std::string& name, float x, float y) const
{
  glProgramUniform2f(program_id, glGetUniformLocation(program_id, name.c_str()), x, y);
}

void Shader::setVec3(const std::string& name, const glm::vec3& value) const
{
  glProgramUniform3fv(program_id, glGetUniformLocation(program_id, name.c_str()), 1, &value[0]);
}

void Shader::setVec3(const std::string& name, float x, float y, float z) const
{
  glProgramUniform3f(program_id, glGetUniformLocation(program_id, name.c_str()), x, y, z);
}

void Shader::setVec4(const std::string& name, const glm::vec4& value) const
{
  glProgramUniform4fv(program_id, glGetUniformLocation(program_id, name.c_str()), 1, &value[0]);
}

void Shader::setVec4(const std::string& name, float x, float y, float z, float w) const
{
  glProgramUniform4f(program_id, glGetUniformLocation(program_id, name.c_str()), x, y, z, w);
}

void Shader::setMat2(const std::string& name, const glm::mat2& mat) const
{
  glProgramUniformMatrix2fv(program_id, glGetUniformLocation(program_id, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::setMat3(const std::string& name, const glm::mat3& mat) const
{
  glProgramUniformMatrix3fv(program_id, glGetUniformLocation(program_id, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::setMat4(const std::string& name, const glm::mat4& mat) const
{
  glProgramUniformMatrix4fv(program_id, glGetUniformLocation(program_id, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::setUVec2(const std::string& name, const glm::uvec2& value) const
{
  glProgramUniform2uiv(program_id, glGetUniformLocation(program_id, name.c_str()), 1, &value[0]);
}

void Shader::setUVec3(const std::string& name, const glm::uvec3& value) const
{
  glProgramUniform3uiv(program_id, glGetUniformLocation(program_id, name.c_str()), 1, &value[0]);
}

void Shader::setUVec4(const std::string& name, const glm::uvec4& value) const
{
  glProgramUniform4uiv(program_id, glGetUniformLocation(program_id, name.c_str()), 1, &value[0]);
}

void Shader::setUVec2(const std::string& name, uint32_t x, uint32_t y) const
{
  glProgramUniform2ui(program_id, glGetUniformLocation(program_id, name.c_str()), x, y);
}

void Shader::setUVec3(const std::string& name, uint32_t x, uint32_t y, uint32_t z) const
{
  glProgramUniform3ui(program_id, glGetUniformLocation(program_id, name.c_str()), x, y, z);
}

void Shader::setUVec4(const std::string& name, uint32_t x, uint32_t y, uint32_t z, uint32_t w) const
{
  glProgramUniform4ui(program_id, glGetUniformLocation(program_id, name.c_str()), x, y, z, w);
}

void Shader::setFloatArray(const std::string& name, const float array[], int count) const
{
  glProgramUniform1fv(program_id, glGetUniformLocation(program_id, name.c_str()), count, array);
}
}  // namespace scigl_render
