/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <scigl_render/shader/shader_builder.hpp>
#include <stdexcept>

namespace scigl_render
{
ShaderBuilder::ShaderBuilder()
{
  vertex_shader = 0;
  fragment_shader = 0;
  tc_shader = 0;
  te_shader = 0;
  geometry_shader = 0;
  compute_shader = 0;
}

void ShaderBuilder::attach_vertex_shader(const std::string& vs_source)
{
  vertex_shader = compile_shader(vs_source, GL_VERTEX_SHADER);
}

void ShaderBuilder::attach_fragment_shader(const std::string& fs_source)
{
  fragment_shader = compile_shader(fs_source, GL_FRAGMENT_SHADER);
}

void ShaderBuilder::attach_geometry_shader(const std::string& gs_source)
{
  geometry_shader = compile_shader(gs_source, GL_GEOMETRY_SHADER);
}

void ShaderBuilder::attach_tessellation_shader(const std::string& tcs_source, const std::string& tes_source)
{
  tc_shader = compile_shader(tcs_source, GL_TESS_CONTROL_SHADER);
  te_shader = compile_shader(tes_source, GL_TESS_EVALUATION_SHADER);
}

void ShaderBuilder::attach_compute_shader(const std::string& cs_source)
{
  compute_shader = compile_shader(cs_source, GL_COMPUTE_SHADER);
}

Shader ShaderBuilder::build()
{
  // shader program
  GLuint program_id = glCreateProgram();
  if (vertex_shader != 0)
  {
    glAttachShader(program_id, vertex_shader);
  }
  if (fragment_shader != 0)
  {
    glAttachShader(program_id, fragment_shader);
  }
  if (geometry_shader != 0)
  {
    glAttachShader(program_id, geometry_shader);
  }
  if (tc_shader != 0 && te_shader != 0)
  {
    glAttachShader(program_id, tc_shader);
    glAttachShader(program_id, te_shader);
  }
  if (compute_shader != 0)
  {
    glAttachShader(program_id, compute_shader);
  }
  glLinkProgram(program_id);
  checkProgramCompileErrors(program_id);
  // Delete unused resources
  glDeleteShader(vertex_shader);
  glDeleteShader(fragment_shader);
  if (geometry_shader != 0)
  {
    glDeleteShader(geometry_shader);
  }
  if (tc_shader != 0)
  {
    glDeleteShader(tc_shader);
  }
  if (te_shader != 0)
  {
    glDeleteShader(te_shader);
  }
  if (compute_shader != 0)
  {
    glDeleteShader(compute_shader);
  }
  return Shader(program_id);
}

GLuint ShaderBuilder::compile_shader(const std::string& source, GLenum shader_type)
{
  if (!source.empty())
  {
    GLuint shader_id = glCreateShader(shader_type);
    // Cast string to const GLchar* since OpenGL expects const GLchar**
    const GLchar* gl_source = source.c_str();
    glShaderSource(shader_id, 1, &gl_source, NULL);
    glCompileShader(shader_id);
    checkShaderCompileErrors(shader_id, source);
    return shader_id;
  }
  else
  {
    return 0;
  }
}

void ShaderBuilder::checkShaderCompileErrors(GLuint shader, const std::string& source)
{
  GLint success;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
  if (!success)
  {
    GLchar infoLog[1024];
    glGetShaderInfoLog(shader, 1024, NULL, infoLog);
    throw std::runtime_error("Shader compilation error"
                             "\nShaderInfoLog: " +
                             std::string(infoLog) + "\nShader source:\n" + source);
  }
}

void ShaderBuilder::checkProgramCompileErrors(GLuint program)
{
  GLint success;
  glGetProgramiv(program, GL_LINK_STATUS, &success);
  if (!success)
  {
    GLchar infoLog[1024];
    glGetProgramInfoLog(program, 1024, NULL, infoLog);
    throw std::runtime_error("Program linking error\nInfoLog: " + std::string(infoLog));
  }
}
}  // namespace scigl_render
