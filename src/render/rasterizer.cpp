/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <GL/gl3w.h>
#include <scigl_render/render/rasterizer.hpp>
#include <sstream>

namespace scigl_render
{
Rasterizer::Rasterizer()
{
}

Rasterizer::Rasterizer(size_t width, size_t height, size_t views_per_row, size_t views_per_column)
{
  set_view_width(width);
  set_view_height(height);
  set_views_per_row(views_per_row);
  set_views_per_column(views_per_column);
}

void Rasterizer::activate_view(size_t row, size_t column) const
{
  auto xy = view_xy(row, column);
  glScissor(xy.first, xy.second, width, height);
  glViewport(xy.first, xy.second, width, height);
}

void Rasterizer::activate_view(size_t view) const
{
  auto position = view_row_column(view);
  size_t row = position.first;
  size_t column = position.second;
  activate_view(row, column);
}

void Rasterizer::activate_all() const
{
  glScissor(0, 0, texture_width, texture_height);
  glViewport(0, 0, texture_width, texture_height);
}

std::pair<size_t, size_t> Rasterizer::calc_min_raster(size_t n_views, size_t width, size_t height,
                                                      size_t max_buffer_size)
{
  // determine maximum number of views
  size_t max_per_row = max_buffer_size / width;
  size_t max_per_column = max_buffer_size / height;
  if (n_views <= 0 || n_views > max_per_row * max_per_column)
  {
    return std::pair<size_t, size_t>(0, 0);
  }
  // minimum needed views per column and row (ceil int division)
  // https://stackoverflow.com/questions/2745074/fast-ceiling-of-an-integer-division-in-c-c
  size_t min_per_column = 1 + ((n_views - 1) / max_per_row);
  size_t min_per_row = 1 + ((n_views - 1) / min_per_column);
  return std::pair<size_t, size_t>(min_per_row, min_per_column);
}

size_t Rasterizer::get_views_per_row() const
{
  return views_per_row;
}
size_t Rasterizer::get_views_per_column() const
{
  return views_per_column;
}
size_t Rasterizer::get_view_width() const
{
  return width;
}
size_t Rasterizer::get_view_height() const
{
  return height;
}
size_t Rasterizer::get_texture_width() const
{
  return texture_width;
}
size_t Rasterizer::get_texture_height() const
{
  return texture_height;
}
std::pair<size_t, size_t> Rasterizer::get_view_xy(size_t view) const
{
  auto row_column = view_row_column(view);
  return view_xy(row_column.first, row_column.second);
}
size_t Rasterizer::get_view_size() const
{
  return get_view_width() * get_view_height();
}
void Rasterizer::set_views_per_row(size_t views_per_row)
{
  this->views_per_row = views_per_row;
  texture_width = views_per_row * width;
}
void Rasterizer::set_views_per_column(size_t views_per_column)
{
  this->views_per_column = views_per_column;
  texture_height = views_per_column * height;
}
void Rasterizer::set_view_width(size_t width)
{
  this->width = width;
  texture_width = views_per_row * width;
}
void Rasterizer::set_view_height(size_t height)
{
  this->height = height;
  texture_height = views_per_column * height;
}

std::pair<size_t, size_t> Rasterizer::view_row_column(size_t view) const
{
  if (view > views_per_row * views_per_column)
  {
    std::stringstream error_stream;
    error_stream << "view number exceeds the raster size " << view << " > " << views_per_row * views_per_column;
    throw std::invalid_argument(error_stream.str());
  }
  size_t row = view / views_per_row;
  size_t column = view % views_per_row;
  return std::pair<size_t, size_t>(row, column);
}

std::pair<size_t, size_t> Rasterizer::view_xy(size_t row, size_t column) const
{
  return std::pair<size_t, size_t>(column * width, row * height);
}
}  // namespace scigl_render