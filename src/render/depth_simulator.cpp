/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <scigl_render/render/depth_simulator.hpp>
#include <scigl_render/check_gl_error.hpp>
#include <scigl_render/shader/shader_builder.hpp>

namespace scigl_render
{
const GLenum DepthSimulator::FORMAT = GL_RED;
const GLenum DepthSimulator::TYPE = GL_FLOAT;
const GLenum DepthSimulator::INTERNAL_FORMAT = GL_R32F;
const size_t DepthSimulator::CHANNELS = 1;
const size_t DepthSimulator::PIXEL_SIZE = CHANNELS * sizeof(GLfloat);

DepthSimulator::DepthSimulator(scigl_render::CvCamera camera, scigl_render::Model model)
  : camera(std::move(camera)), model(std::move(model))
{
  const std::string vs_source =
#include <scigl_render/shader/simple.vert>
      "";
  const std::string fs_source =
#include <scigl_render/shader/depth.frag>
      "";
  scigl_render::ShaderBuilder shader_builder;
  shader_builder.attach_vertex_shader(vs_source);
  shader_builder.attach_fragment_shader(fs_source);
  depth_shader = shader_builder.build();
  scigl_render::check_gl_error("depth simulator created");
}

int DepthSimulator::get_width()
{
  return camera.get_intrinsics().width;
}

int DepthSimulator::get_height()
{
  return camera.get_intrinsics().height;
}

void DepthSimulator::render_pose(const QuaternionPose& object_pose, const QuaternionPose& camera_pose)
{
  glColorMask(GL_TRUE, GL_FALSE, GL_FALSE, GL_FALSE);
  glEnable(GL_DEPTH_TEST);
  camera.pose = camera_pose;
  camera.set_in_shader(depth_shader);
  model.pose = object_pose;
  depth_shader.activate();
  model.draw(depth_shader);
  depth_shader.deactivate();
}
}  // namespace scigl_render