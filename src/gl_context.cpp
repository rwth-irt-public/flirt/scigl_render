/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <algorithm>
#include <functional>
#include <iostream>
#include <scigl_render/gl_context.hpp>
#include <stdexcept>

void glfw_error_callback(int error, const char* description)
{
  std::cerr << "GLFW Error code " << error << " description: " << description << std::endl;
}

namespace scigl_render
{
GLContext::GLContext(bool visible, bool fullscreen, int width, int height, int version_major, int version_minor)
  : width(width), height(height)
{
  glfwSetErrorCallback(&glfw_error_callback);
  glfwInit();
  // compability requirements
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, version_major);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, version_minor);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  if (!visible)
  {
    glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
  }
  if (fullscreen)
  {
    window = glfwCreateWindow(width, height, "My Title", glfwGetPrimaryMonitor(), NULL);
  }
  else
  {
    window = glfwCreateWindow(width, height, "My Title", NULL, NULL);
  }
  if (!window)
  {
    throw std::runtime_error("Failed to create glfw context");
  }
  glfwMakeContextCurrent(window);
  // load OpenGl core functionaliy
  if (gl3wInit())
  {
    throw std::runtime_error("Failed to initialize OpenGL");
  }
  if (!gl3wIsSupported(version_major, version_minor))
  {
    throw std::runtime_error("OpenGL " + std::to_string(version_major) + "." + std::to_string(version_minor) +
                             " not supported");
  }
  std::cout << "OpenGL Version: " << glGetString(GL_VERSION)
            << " GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n";
  std::cout << "width " << width << " height " << height << "\n";
  glViewport(0, 0, width, height);
}

GLContext::~GLContext()
{
  glfwDestroyWindow(window);
  glfwTerminate();
  std::cout << "destroyed the contex\n";
}

GLFWwindow* GLContext::get_window()
{
  return window;
}

int GLContext::get_width()
{
  return width;
}

int GLContext::get_height()
{
  return height;
}

bool GLContext::is_fullscreen()
{
  // if non null a monitor is active in fullscreen
  return static_cast<bool>(glfwGetWindowMonitor(window));
}

void GLContext::set_fullscreen(bool enabled)
{
  // switch to fullscreen
  if (enabled && !is_fullscreen())
  {
    auto monitor = find_current_monitor();
    auto mode = glfwGetVideoMode(monitor);
    int x_pos, y_pos;
    glfwGetMonitorPos(monitor, &x_pos, &y_pos);
    glfwSetWindowMonitor(window, monitor, 0, 0, width, height, mode->refreshRate);
  }
  // switch to windowed
  if (!enabled && is_fullscreen())
  {
    auto monitor = glfwGetWindowMonitor(window);
    int x_pos, y_pos;
    glfwGetMonitorPos(monitor, &x_pos, &y_pos);
    glfwSetWindowMonitor(window, NULL, x_pos, y_pos, width, height, GLFW_DONT_CARE);
  }
  glfwSwapBuffers(window);
}

void GLContext::set_resolution(int width, int height)
{
  this->width = width;
  this->height = height;
  glfwSetWindowSize(window, width, height);
  std::cout << "width " << width << " height " << height << "\n";
  glViewport(0, 0, width, height);
}

void GLContext::set_monitor_resolution()
{
  GLFWmonitor* monitor;
  if (is_fullscreen())
  {
    monitor = glfwGetWindowMonitor(window);
  }
  else
  {
    monitor = find_current_monitor();
  }
  auto mode = glfwGetVideoMode(monitor);
  set_resolution(mode->width, mode->height);
}

void GLContext::toggle_fullscreen()
{
  if (is_fullscreen())
  {
    set_fullscreen(false);
  }
  else
  {
    set_fullscreen(true);
  }
}

GLFWmonitor* GLContext::find_current_monitor()
{
  // window properties
  int window_x, window_y;
  glfwGetWindowPos(window, &window_x, &window_y);
  int window_width, window_height;
  glfwGetWindowSize(window, &window_width, &window_height);
  GLFWmonitor** monitors;
  // the winner
  int largest_area = 0;
  GLFWmonitor* best_monitor = NULL;
  int n_monitors;
  monitors = glfwGetMonitors(&n_monitors);
  for (int i = 0; i < n_monitors; i++)
  {
    // monitor properties
    const GLFWvidmode* mode;
    mode = glfwGetVideoMode(monitors[i]);
    int monitor_x, monitor_y;
    glfwGetMonitorPos(monitors[i], &monitor_x, &monitor_y);
    int monitor_width = mode->width;
    int monitor_height = mode->height;
    // overlapping rectangle on this monitor
    int left = std::max(window_x, monitor_x);
    int right = std::min(window_x + window_width, monitor_x + monitor_width);
    int top = std::max(window_y, monitor_y);
    int bottom = std::min(window_y + window_height, monitor_y + monitor_height);
    int overlap_area = std::max(right - left, 0) * std::max(bottom - top, 0);
    if (largest_area < overlap_area)
    {
      largest_area = overlap_area;
      best_monitor = monitors[i];
    }
  }
  return best_monitor;
}

}  // namespace scigl_render